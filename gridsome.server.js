// Server API makes it possible to hook into various parts of Gridsome
// on server-side and add custom data to the GraphQL data layer.
// Learn more: https://gridsome.org/docs/server-api/

// Changes here require a server restart.
// To restart press CTRL + C in terminal and run `gridsome develop`

const {
  GraphQLInt
} = require('gridsome/graphql')

module.exports = function (api) {
  api.loadSource(async actions => {
    // Global metadata
    actions.addMetadata('siteName', 'Pewen')
    actions.addMetadata('siteUrl', 'https://pewen.gitlab.io')

    //
    actions.addSchemaTypes(`
      type Post implements Node @infer {
        timeToRead: Int
      }
    `)

    actions.addSchemaResolvers({
      Post: {
        timeToRead: {
          type: GraphQLInt,
          args: {
            speed: {
              type: GraphQLInt,
              description: 'Words per minute',
              defaultValue: 200
            }
          },
          resolve: (node, { speed }) => {
            const count = node.content.split(/ /).length
            return Math.round(count / speed) || 1
          }
        }
      }
    })
  })
}
