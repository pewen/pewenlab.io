import VueI18n from 'vue-i18n'
/**
 * i18n client plugin initialization
 *
 * @param Vue
 * @param options
 */

export default function (Vue, options, { appOptions, router }) {
  Vue.use(VueI18n)

  // Setup options fallback
  options.defaultLocale = options.defaultLocale || options.locales[0]
  options.fallbackLocale = options.fallbackLocale || options.defaultLocale

  const i18n = new VueI18n(Object.assign(options, {
    locale: options.defaultLocale
  }))
  // All the locales
  i18n.locales = options.locales
  appOptions.i18n = i18n

  // Object with all the paths
  const allPaths = {}
  options.locales.forEach(locale => { allPaths[locale] = {} })
  router.options.routes.forEach(route => {
    const { locale, originalPath, translatePath } = route.meta
    allPaths[locale][originalPath] = translatePath
  })

  // Given a route, return the route locale
  Vue.prototype.localePath = function (route, locale) {
    // Abort if no route or no locale
    if (!route) {
      return
    }

    const { $i18n: i18n } = this

    locale = locale || i18n.locale

    if (!locale) {
      return
    }

    // If route parameter is a string, check if it's a path or name of route.
    if (typeof route === 'string') {
      if (route[0] === '/') {
        // If route parameter is a path, create route object with path.
        route = route.endsWith('/') ? route : `${route}/`
        route = { path: route }
      } else {
        // Else use it as route name.
        route = { name: route }
      }
    }

    if (route.path && !route.name) {
      const path = allPaths[locale][route.path]
      return path
    }

    console.log('INVALID PATH', route)
    // TODO: aceptar que puedan pasar route names
    // const isPrefixed = locale !== defaultLocale
    // const path = (isPrefixed ? `/${locale}${route.path}` : route.path)
    //
    // return path
  }

  // Translate the url
  Vue.prototype.switchLocalePath = function (locale) {
    const { locales } = options

    let path
    if (locale in this.$context.pathTranslate) {
      path = this.$context.pathTranslate[locale]
    } else {
      let pathSplit = this.$route.path.split('/')
      pathSplit = locales.includes(pathSplit[1]) ? pathSplit.slice(2, pathSplit.length - 2)
        : pathSplit.slice(0, pathSplit.length - 2)
      path = `/${locale}/${pathSplit.join('/')}/`
    }

    this.$i18n.locale = locale
    this.$router.replace(path)

    return path
  }
}
