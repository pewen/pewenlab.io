const path = require('path')
const { extractComponentOptions } = require('./helpers/components')

// https://github.com/daaru00/gridsome-plugin-i18n
// https://github.com/menteora/gridsome-blog

const newPage = (createPage, page, path, route, locale, pathTranslate) => {
  createPage({
    path: path,
    component: route.component,
    context: Object.assign({
      locale: locale,
      pathTranslate: pathTranslate
    }, page.context || {}),
    queryVariables: page.internal.queryVariables || {},
    route: {
      meta: Object.assign({
        locale: locale,
        originalPath: route.path,
        translatePath: path
      }, route.internal.meta || {})
    }
  })
}

class VueI18n {
  /**
   * Default plugin options
   */
  static defaultOptions () {
    return {
      locales: [],
      defaultLocale: null,
      i18n: {}
    }
  }

  /**
   *
   * @param {*} api
   * @param {*} options
   */
  constructor (api, options) {
    this.api = api
    this.pages = api._app.pages
    this.options = options

    const defaultLocale = options.defaultLocale || options.locales[0]

    this.options.defaultLocale = defaultLocale
    api.createManagedPages(this.createManagedPages.bind(this))
  }

  /**
   * Create manage pages
   *
   * @param {function} param.findPages
   * @param {function} param.createPage
   * @param {function} param.removePage
   */
  async createManagedPages ({ findPages, createPage, removePage }) {
    // List all pages
    const pages = findPages()
    const { defaultLocale, locales } = this.options

    const isMultiLangArray = pages.map(page => {
      const queryVariables = page.internal.queryVariables || {}
      const isMultiLang = !Object.keys(queryVariables).includes('lang')
      return isMultiLang
    })

    const multiLangPages = pages.filter((page, i) => isMultiLangArray[i])
    const singleLangPages = pages.filter((page, i) => !isMultiLangArray[i])

    console.log('multiLangPages')
    for (const page of multiLangPages) {
      // Load page's route
      const route = this.pages.getRoute(page.internal.route)
      const pageOptions = extractComponentOptions(route.component)

      const pathTranslate = {}
      if (Object.keys(pageOptions).includes('paths')) {
        // Load custom path from the page
        Object.keys(pageOptions.paths).forEach(locale => {
          const isDefault = locale === defaultLocale
          const newPath = isDefault ? pageOptions.paths[locale] : path.join(`/${locale}/`, pageOptions.paths[locale])
          pathTranslate[locale] = newPath
        })
      } else {
        locales.forEach(locale => {
          pathTranslate[locale] = path.join(`/${locale}/`, route.path)
        })
      }

      Object.keys(pathTranslate).forEach(locale => {
        newPage(createPage, page, pathTranslate[locale], route, locale, pathTranslate)
        console.log(locale, pathTranslate[locale])
      })

      // Set default locale on pages without locale segment
      const oldPage = Object.assign({}, page)
      removePage(page)
      newPage(createPage, oldPage, route.path, route, defaultLocale,
        pathTranslate)
      console.log(defaultLocale, route.path)
    }

    console.log('singleLangPages')
    for (const page of singleLangPages) {
      // Load page's route
      const route = this.pages.getRoute(page.internal.route)

      const oldPage = Object.assign({}, page)
      removePage(page)

      // TODO
      // Si el lang existe, chequear que sea valido
      const locale = oldPage.internal.queryVariables.lang || this.options.defaultLocale
      const pathTranslate = oldPage.internal.queryVariables.translate || {}

      console.log(locale, route.path)
      newPage(createPage, oldPage, route.path, route, locale, pathTranslate)

      // Set default locale on pages without locale segment
      if (locale === defaultLocale) {
        let pagePath = route.path.split('/')
        pagePath = pagePath.slice(2, pagePath.length).join('/')
        pagePath = `/${pagePath}`
        console.log(locale, pagePath)

        newPage(createPage, oldPage, pagePath, route, locale, pathTranslate)
      }
    }
  }
}

module.exports = VueI18n
