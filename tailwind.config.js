module.exports = {
  theme: {
    fontFamily: {
      'body': ['"Roboto"'],
      'mono': ['"Chakra Petch"', 'Menlo', 'Monaco', 'Consolas']
    },
    zIndex: {
      '1000': 1000
    },
    inset: {
      '0': 0,
      'auto': 'auto',
      '1/4': '25%'
    },
    boxShadow: {
      'default': '0 1px 3px 0 rgba(0, 0, 0, .1), 0 1px 2px 0 rgba(0, 0, 0, .06)',
      'md': ' 0 4px 6px -1px rgba(0, 0, 0, .1), 0 2px 4px -1px rgba(0, 0, 0, .06)',
      'lg': ' 0 10px 15px -3px rgba(0, 0, 0, .1), 0 4px 6px -2px rgba(0, 0, 0, .05)',
      'xl': ' 0 20px 25px -5px rgba(0, 0, 0, .1), 0 10px 10px -5px rgba(0, 0, 0, .04)',
      '2xl': '0 25px 50px -12px rgba(0, 0, 0, .25)',
      '3xl': '0 35px 60px -15px rgba(0, 0, 0, .3)',
      'nav-bar': '0px 5px 5px rgba(0, 0, 0, 0.6)',
      'footer': '0px -5px 10px rgba(0, 0, 0, 0.5)',
      'project': '10px 10px 2px 1px rgba(0, 0, 255, .2)'
    },
    extend: {
      colors: {
        background: {
          'body': 'var(--background-body)',
          'body-secondary': 'var(--background-body-secondary)',

          'primary': 'var(--background-primary)',
          'primary-light': 'var(--background-primary-light)',
          'primary-dark': 'var(--background-primary-dark)',

          'secondary': 'var(--background-secondary)',
          'thernary': 'var(--background-thernary)',

          'on-body': 'var(--text-on-body)'
        }
      },
      textColor: {
        'on-primary': 'var(--text-on-primary)',
        'on-primary-light': 'var(--text-on-primary-light)',
        'on-body': 'var(--text-on-body)',
      },
      borderColor: {
        'primary-dark': 'var(--background-primary-dark)',

        'on-body': 'var(--text-on-body)'
      },
      borderWidth: {
        '10': '10px'
      },
      fill: theme => ({
       'red': theme('colors.red.500'),
       'green': theme('colors.green.500'),
       'blue': theme('colors.blue.500'),
       'white': '#ffffff',
       'gitlab': '#fa7035'
      }),
      gridTemplateRows: {
        'post-banner': 'minmax(20%, 40%) auto auto minmax(5%, 40%)',
        'blog-entry-mobile': 'auto 3fr',
        'card': '.2fr minmax(120px, auto) .2fr'
      }
    }
  },
  variants: {
    fill: ['responsive', 'hover', 'focus'],
    borderWidth: ['responsive', 'hover', 'focus']
  },
  plugins: [],
}
