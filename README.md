# Pewen Web Site

## Run

```bash
$ docker-compose up # For develop mode
$ docker-compose run --rm web yarn build # For build the site
```

## i18n

`localePath` – Returns the localized URL for a given page.
```js
<g-link :to="localePath('/blog')">
  {{ $t('navbar.blog') }}
</g-link>
```

`switchLocalePath` – Returns a link to the current page in another language
```js
this.switchLocalePath('es')
```

### Custom path name

```
// pages/Projects.vue

export default {
  gridsomeI18n: {
    paths: {
      es: '/proyectos', // -> accessible at /proyectos (no prefix since it's the default locale)
      en: '/projects', // -> accessible at /en/projects
      pt: '/projetos' // -> accessible at /pt/projetos
    }
  }
}
```

### TODO

- [ ] SEO
- [ ] Mejor interfaz con los md
- [ ] Usar vue minix
- [ ] Opcional usar vuex y/o cookie
- [ ] Actualizar package.json

## Add Icons

Los iconos los descargamos de [https://feathericons.com/](https://feathericons.com/), los dejamos en `src/assets/svg` y los podemos importar como componentes directamente.

```js
// SomeVueComponen.js

import Sun from '~/assets/svg/sun.svg'

export default {
  components: {
    Sun
  }
}
```

Tenemos que borrar el width y el height del svg si lo tienen.


## Add feature

Para los archivos vue tratamos de seguir la [guia de estilo de vue](https://vuejs.org/v2/style-guide/). Para el js usamos [standardJs](https://standardjs.com/) que ya esta integrado en el linter.

## Performance

Para medir la performance del website usamos [https://web.dev/](https://web.dev/).

| Performance | Accessibility | Best Practices | SEO | Milestone   |
| ----------- |:-------------:| --------------:| ---:| -----------:|
| 66          | 41            | 92             | 91  | GZIP assets |

## TODO

  - [ ] Mini doc
  - [ ] UI/UX
    - [ ] Agregar autores de los post
    - [ ] Logo
    - [ ] Banner en el home
    - [x] Licencia en el footer
    - [x] Gitlab en el footer
    - [ ] Transicion
    - [x] Mejor modo nocturno
    - [ ] Paleta de colores para los plots
  - [ ] Plot container
    - [ ] Descargar los datos
    - [ ] Descargar png del plot
    - [ ] Embeber el plot
  - [x] Mapas
    - [x] Nocturno sin nombres (wiki)
    - [x] Nocturno con nombres (sismos)
    - [x] Dia sin nombre (wiki)
    - [x] Dia con nombre (sismos)
  - [ ] SEO
    - [ ] Lang
    - [ ] OpenGraph
      - [ ] `article:author`
      - [ ] `article:tag`
      - [ ] Validar
    - [ ] Twitter card
      - [ ] `twitter:creator`
      - [ ] `twitter:image`
      - [ ] Validar
    - [ ] JSON+LD
      - [ ] image
      - [ ] pewen logo
      - [ ] authors
      - [ ] Validar
    - [ ] OpenGraph, twitter card and JSON+LD for not post pages
  - [ ] `/plot` Pagina con los plots que tenemos y como usarlos
  - [ ] [Comprimir el resultado](https://webpack.js.org/plugins/compression-webpack-plugin/)
  - [ ] i18n
    - [x] Crear plugin custom
    - [ ] Resolver link del header
    - [ ] Publicar plugin
  - [ ] TOML files
    - [x] Crear plugin
    - [ ] watch files
    - [ ] Transformar campos especificos con md usando remark-html
    - [ ] publicar plugin
  - [x] "Dockerisar"
  - [x] Usar [eslint](https://eslint.org/) con [standardJs](https://standardjs.com/)
  - [ ] Usar [prettier]?(https://prettier.io/)
  - [ ] Test de url mediante wget
  - [ ] Test de accesibilidad

## Historias
  - [ ] Analisis tweets 8M
  - [ ] Precios en ML
  - [ ] Distancia de viaje (Isochrone map)

## Herramientas
  - [ ] Completar
    - [ ] Generificador
    - [ ] Edad desde el num doc
    - [ ] Normalizar nombre de calle
    - [ ] Calles a [lat, lng]

## Dataset
  - [ ] Codigo postales

## New TODO

  - Calculo del timepo de lectura
  - wikimapa mujeres
    - Resaltar el punto cuando te pares en el top de nombres
    - Poner la descripcion de la pagina de la wiki cuando on-hover de alguno del top de nombres
    - Usar una escala mas grandes para los puntos cuando tengas mucho zoom (son todos iguales sino)
