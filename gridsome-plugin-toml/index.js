const fs = require('fs')
const path = require('path')
const toml = require('toml')

const isDev = process.env.NODE_ENV === 'development'

class TomlLoader {
  constructor (api, options) {
    this.api = api
    this.options = options
    this.context = options.baseDir
      ? api.resolve(options.baseDir)
      : api.context

    api.loadSource(async actions => {
      this.createCollections(actions)
      await this.createNodes(actions)
      if (isDev) this.watchFiles(actions)
    })
  }

  createCollections (actions) {
    const addCollection = actions.addCollection || actions.addContentType

    this.collection = addCollection({
      typeName: this.options.typeName
    })
  }

  createNodes (actions) {
    fs.readdirSync(this.context).forEach(fileName => {
      // Load the proyect's TOML file
      const filePath = path.join(this.context, fileName)
      const fileStreaming = fs.readFileSync(filePath, 'utf-8')
      const data = toml.parse(fileStreaming)

      Object.keys(data).forEach(key => {
        const node = data[key]
        node.fileName = fileName
        this.collection.addNode(data[key])
      })
    })
  }

  watchFiles (actions) {
    const chokidar = require('chokidar')

    const watcher = chokidar.watch(this.context, {
      persistent: true
    })

    const log = console.log.bind(console)
    // Add event listeners.
    watcher
      .on('add', path => log(`File ${path} has been added`))
      .on('change', path => log(`File ${path} has been changed`))
      .on('unlink', path => log(`File ${path} has been removed`))
  }
}

module.exports = TomlLoader
