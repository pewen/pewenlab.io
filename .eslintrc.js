module.exports = {
  parser: "vue-eslint-parser",
  parserOptions: {
    ecmaVersion: 2018,
    sourceType: "module",
    ecmaFeatures: {
      jsx: true
    }
  },
  env: {
    browser: true,
    es6: true
  },
  plugins: ["gridsome"],
  extends: [
    "standard",
    'plugin:vue/vue3-recommended'
  ],
  rules: {
    "vue/max-attributes-per-line": ["error", {
      "singleline": 3,
      "multiline": {
        "max": 3,
        "allowFirstLine": true
      }
    }],
    "vue/component-tags-order": ["error", {
      "order": ["template", "static-query", "script", "style"]
    }]
  }
}
