// This is where project configuration and plugin options are located.
// Learn more: https://gridsome.org/docs/config

// Changes here require a server restart.
// To restart press CTRL + C in terminal and run `gridsome develop`

const CompressionPlugin = require('compression-webpack-plugin')
const BundleAnalyzerPlugin = require('webpack-bundle-analyzer').BundleAnalyzerPlugin

module.exports = {
  host: '0.0.0.0',
  port: 8080,
  siteName: 'Pewen',
  siteUrl: 'https://pewen.gitlab.io/',
  outputDir: 'public',
  plugins: [
    '@gridsome/plugin-sitemap',
    'gridsome-plugin-robots-txt',
    'gridsome-plugin-tailwindcss',

    {
      use: 'gridsome-plugin-matomo',
      options: {
        host: '//metricas.ciap.org.ar/',
        siteId: '2'
      }
    },

    // Load markdown posts
    {
      use: '@gridsome/vue-remark',
      options: {
        typeName: 'Post',
        baseDir: './blog/', // Where .md files are located
        route: '/:lang/blog/:title',
        template: './src/templates/Post.vue'
      }
    },

    // Projects
    {
      use: 'gridsome-plugin-toml',
      options: {
        typeName: 'Projects',
        baseDir: './src/data/projects/'
      }
    },

    // Datasets
    {
      use: 'gridsome-plugin-toml',
      options: {
        typeName: 'Datasets',
        baseDir: './src/data/datasets/'
      }
    },

    // i18n plugin
    {
      use: 'gridsome-plugin-i18n-c',
      options: {
        locales: [
          'es',
          'en',
          'pt'
        ],
        defaultLocale: 'es', // default language
        messages: {
          es: require('./locales/es-AR.json'),
          en: require('./locales/en-US.json'),
          pt: require('./locales/pt-BR.json')
        }
      }
    }
  ],
  chainWebpack: config => {
    const svgRule = config.module.rule('svg')
    svgRule.uses.clear()
    svgRule
      .use('vue-svg-loader')
      .loader('vue-svg-loader')

    const gzipPlugin = config.plugin('gzip')
    gzipPlugin
      .use(CompressionPlugin, [{
        filename: '[path].gz[query]',
        algorithm: 'gzip',
        test: /\.js$|\.css$|\.html$/,
        compressionOptions: { level: 9 },
        // threshold: 10240,
        minRatio: 0.3,
        deleteOriginalAssets: false
      }])

    const brotliPlugin = config.plugin('brotli')
    brotliPlugin
      .use(CompressionPlugin, [{
        filename: '[path].br[query]',
        algorithm: 'brotliCompress',
        test: /\.(js|css|html|svg)$/,
        compressionOptions: { level: 11 },
        // threshold: 10240,
        minRatio: 0.3,
        deleteOriginalAssets: false
      }])

    const analyzerPlugin = config.plugin('analyzerPlugin')
    analyzerPlugin
      .use(BundleAnalyzerPlugin, [{ analyzerMode: 'static' }])
  }
}
