---
lang: "es"
date: "2020-04-22"
title: "Sismos en Vaca Muerta (Añelo, Sauzal Bonito y la región)"
plots: 2
tags: ["number"]
summary: "Actividad sismica en zona de Fracking. Visualización en el mapa de la actividad sismica en Vaca Muerta."

authors:
  - name: "Franco N. Bellomo"
    twitter: "fnbellomo"
  - name: "Lucas E. Bellomo"
    twitter: "ucaomo"
  - name: "Azul Barbeito"
    facebook: "azul.barbeito"
---
import MapSismos from '~/components/PlotMapSismos'
import PlotScatter from '~/components/PlotScatter'

## Actividad sismica en zona de Fracking

En [Sauzal Bonito](https://es.wikipedia.org/wiki/Sauzal_Bonito) los [vecinos están sufriendo un aumento de la actividad sísmica]
(https://www.laizquierdadiario.com/Sismos-en-Vaca-Muerta-Tengo-miedo-de-que-a-mis-nietos-les-pase-algo),
lo que se podría  relacionar con la práctica del fracking en la zona.
Existen registros que indican que  ese método de extracción de petróleo produce un aumento en la actividad sísmica, como se ha identificado en Texas,
Nuevo México y Colorado. Los científicos del Servicio Geológico de Estados Unidos (USGS, por sus siglas en inglés) reconocen que los responsables del
aumento en la frecuencia e intesidad de los sismos, registrados en esos estados, es la técnica de manejo de aguas residuales de las empresas que usan
el método de fractura hidráulica (conocido como fracking).

Por eso hemos decidido poner a disposición el siguiente informe gráfico, que permite visualizar el mapa con los sismos en Sauzal Bonito y la región
(nos acotamos a la provincia de Neuquén).
El mismo fue elaborado a partir de datos oficiales del [Instituto Nacional de Prevención Sísmica (INPRES)](https://www.inpres.gob.ar). Además se
realizó la herramienta (software) [Scraper INPRES](https://gitlab.com/pewen/scrapers/inpres) para descargar y estructurar los datos desde la web del INPRES.

### Mapa 

<MapSismos dataUrl="/data/sismos/sismos_nqn.csv" :center="[-38.65, -68.7]" :zoom="10" />

Se utilizan las referencias del INPRES (Instituto Nacional de Prevención Sísmica). En color rojo se representan los sismos que se sintieron y en en azul
los que no fueron percibidos. El tamaño del círculo representa la intensidad del sismo.

### Línea temporal

<PlotScatter dataUrl="/data/sismos/sismos_añelo.csv" />

Línea temporal de los sismos relevados del INPRES. En color rojo se representan los sismos que se sintieron y en en azul
los que no fueron percibidos. El tamaño del círculo representa la intensidad del sismo.

## Datos y metodología

Si bien los datos del INPRES son públicos y se puede acceder a ellos en su sitio web, están presentados de una forma muy técnica, tienen un formato
poco recomendado y se complica descargarlos, todo esto entorpece a la hora de utilizarlos. Por eso hemos decido recopilar esa información y compartirla
en formato abierto como recomiendan los estándares internacionales. Todo esto mejorará el uso y acceso a la información para los vecinos, la sociedad
civil, periodistas, y demás interesados.

- [Scraper para obtener los datos](https://gitlab.com/pewen/scrapers/inpres/)
- [descargar dataset](https://gitlab.com/pewen/scrapers/inpres/-/raw/master/data/inpres/inpres.csv)
- [Notebook con el procesamiento de los datos](https://gitlab.com/pewen/blog-analysis/analysis/-/blob/master/sismos/sismos_preprocessing.ipynb)

## Referencias

- [Economía Sustentable (26/02/2020) "Vaca Muerta y el fracking: una historia de sismos y contaminación que atentan contra poblaciones y ambiente en Neuquén"](https://economiasustentable.com/noticias/vaca-muerta-y-el-fracking-una-historia-de-sismos-y-contaminacion-que-atentan-contra-poblaciones-y-ambiente-en-neuquen)
- [Observatorio Petrolero Sur (23/03/2019) "Sismos en Vaca Muerta, las sospechas recaen en el fracking"](https://www.opsur.org.ar/blog/2019/03/23/sismos-en-vaca-muerta-las-sospechas-recaen-en-el-fracking/)
- [The New York Times (17/11/2016) “En Canadá, un vínculo directo entre Fracking y terremotos”](https://www.nytimes.com/2016/11/18/science/fracking-earthquakes-alberta-canada.html)
- [BBC (16/09/2014) "Vinculan el fracking con el aumento de sismos en Estados Unidos"](https://www.bbc.com/mundo/noticias/2014/09/140916_ciencia_fracking_mas_sismos_estados_unidos_evidencias_np)
