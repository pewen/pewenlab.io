function downloadAsCSV (title, header, data) {
  let body = header.join(',') + '\n'
  data.forEach(row => {
    body += row.join(',')
    body += '\n'
  })

  const hiddenElement = document.createElement('a')
  hiddenElement.href = 'data:text/csv;charset=utf-8,' + encodeURI(body)
  hiddenElement.target = '_blank'
  hiddenElement.download = `${title}.csv`
  hiddenElement.click()
}

export { downloadAsCSV }
