const state = {
  lang: 'es',
  theme: 'light'
}

const getters = {}

const mutations = {
  setTheme (state, payload) {
    state.theme = payload.theme
    localStorage.setItem('theme', payload.theme)
  },
  setLang (state, payload) {
    state.lang = payload.lang
    localStorage.setItem('lang', payload.lang)
  }
}

const actions = {
  setTheme ({ commit }, payload) {
    commit('setTheme', payload)
  },
  toggleTheme ({ commit, state }) {
    const theme = state.theme === 'light' ? 'dark' : 'light'

    commit('setTheme', { theme })
  },
  setLang ({ commit }, payload) {
    commit('setLang', payload)
  }
}

export default {
  namespaced: true,
  state,
  getters,
  mutations,
  actions
}
