FROM node:13.8-buster

RUN apt update

WORKDIR /usr/app

# Install npm deps
COPY package.json yarn.lock ./
RUN yarn install

# Copy local modelues
COPY gridsome-plugin-i18n-c ./gridsome-plugin-i18n-c/
COPY gridsome-plugin-toml ./gridsome-plugin-toml/

# Link local modules
RUN cd /usr/app/gridsome-plugin-i18n-c && yarn link && \
    cd /usr/app/gridsome-plugin-toml && yarn link && \
    cd /usr/app && \
    yarn link gridsome-plugin-i18n-c && \
    yarn link gridsome-plugin-toml

EXPOSE 8080
